import Tween from 'ol'
import Point from 'ol/geom/Point'
import Polygon from 'ol/geom/Polygon';
import Projection from 'ol/proj/Projection';
// import LinearRing from 'ol/geom/LinearRing';
import OpenLayers from 'ol'
import {getVectorContext} from 'ol/render';
import {unByKey} from 'ol/Observable';
import {easeOut} from 'ol/easing';
import {Circle as CircleStyle, Stroke, Style, Fill} from 'ol/style';
import Feature from 'ol/Feature';
import Icon from 'ol/style/Icon';

export function startBackgroundAlarm(environmentSettings,  alarmMarkers, alarmType ){
    /** --------------- design and adding of background rectangle alarm ---------------- */

    
    var polygonCoordinates = [];
    var p1 = [-environmentSettings.x_origin, -environmentSettings.y_origin];
    var p2 = [environmentSettings.width, environmentSettings.y_origin];
    var p3 = [environmentSettings.width,-environmentSettings.height];
    var p4 = [-environmentSettings.y_origin, -environmentSettings.height];
    var p5 = [-environmentSettings.x_origin, -environmentSettings.y_origin];
    
    polygonCoordinates.push(p1, p2, p3, p4, p5);

    //  var linearRingPolygon = LinearRing(polygonCoordinates);
    // console.log({linearRingPolygon});

    var markerBackground = new Feature({
        id: `alarm-background`,
        type: 'Polygon',
        geometry: new Polygon([polygonCoordinates]),
        fillOpacity: 0.01
    });
    
    var markerBackgroundColor = '#fff';
    if (alarmType == 'DANGER'){
        markerBackgroundColor = "rgba(255, 52, 5, 0.59)";
    }else if (alarmType == 'WARNING'){
        markerBackgroundColor= "rgba(255, 141, 5, 0.59)";
    }
    markerBackground.setId(`alarm-background`);
    markerBackground.setStyle(new Style({
       
        stroke: new Stroke({
            color: markerBackgroundColor,
            width: 3,
            opacity: 0.01
        }),
        fill: new Fill({
            color: markerBackgroundColor,
            opacity: 0.01
        }),
    }));

    alarmMarkers.addFeature(markerBackground);
    // console.log('Added background ALARM! ');

}

export function stopBackgroundAlarm(alarmMarkers){
    var featureBackground = alarmMarkers.getFeatureById('alarm-background');
    if(featureBackground){
        alarmMarkers.removeFeature(featureBackground);
        console.log('removed background feature');
    }
}
    /** --------------- design and adding of multicircle visual alarm ---------------- */

// to emulate the blinking, a invisible point is added and remove continuosly from the map
// since the blinking is associated to the addfeature event
export function startBlinkingAlarm(item, sourceMarkers, alarmMarkers ){
    var markerId = item.id;
    var itemInDangerMarker = null;

    // read from source markers a feature to copy
    if(sourceMarkers.getFeatureById(markerId)){
        // copy the item form in order to create a similar feature
        itemInDangerMarker = sourceMarkers.getFeatureById(markerId);
        // delete if alarm for that item already exists
        var featureAlarmToRemove = alarmMarkers.getFeatureById(`invisible-${markerId}`);

        if (featureAlarmToRemove){   
            alarmMarkers.removeFeature(featureAlarmToRemove);
        } 
   
        // copy the geomertry of the target feature to have an invisible feature to add and remove
        
        var invisibleFeatureGeometry = itemInDangerMarker.getGeometry().clone();
        var invisibleFeature = new Feature({
            id: `invisible-${markerId}`,
            geometry: invisibleFeatureGeometry
        });
        invisibleFeature.setId(`invisible-${markerId}`);
        invisibleFeature.setStyle(new Style({
            image: new Icon({
                opacity: 0,
                anchor: [0.5, 0.5],
                scale: 1,
                anchorXUnits: "fraction",
                anchorYUnits: "fraction",
                color: '#fff',
                crossOrigin: 'anonymous',
                src: 'https://openlayers.org/en/v3.20.1/examples/data/dot.png'

            })
        }))
    //    console.log(`Add invisible feature with id ${invisibleFeature.getId()}`);
        alarmMarkers.addFeature(invisibleFeature);
    }
}

// create the event of drawing circle around the item that is in danger
export function initializeBlinkingAlarm( alarmFeatureInv ,layer, map){

    //  It is sure that the feature exixts since this event is called only when a feature is added
        var blinkMarker = alarmFeatureInv.getGeometry().clone();
        const duration = 2000;
        const start = Date.now();
        const listenerKey = layer.on('postrender', function(event){
            const frameState = event.frameState;

            const elapsed = frameState.time - start;
                if (elapsed >= duration) {
                unByKey(listenerKey);
                return;
            }
            const vectorContext = getVectorContext(event);
            const elapsedRatio = elapsed / duration;
            // radius will be 5 at start and 30 at end.
            const radius = easeOut(elapsedRatio) * 25 + 5;
            const opacity = easeOut(1 - elapsedRatio);
            const style = new Style({
                image: new CircleStyle({
                  radius: radius,
                  stroke: new Stroke({
                    color: 'rgba(255, 0, 0, ' + opacity + ')',
                    width: 0.35 + opacity,
                  }),
                }),
            });
            vectorContext.setStyle(style);
            vectorContext.drawGeometry(blinkMarker);
            map.render()
        });
}

export function addAlarmEvent(item, alarmMarkers){
    var toMercator = Projection.transforms['EPSG:4326']['EPSG:3857'];

    var coordinates = item.position.coordinates;
    var alarmFeature = new Feature(
       toMercator(new Point([coordinates.x, coordinates.y])), {}, {
           fillColor:"#A32929",
           strokeColor: "#F1eb3c",
           pointRadius: 8,
           strokeWidht: 5,
           strokeOpacity: 0.2
       }
    );
    alarmFeature.setId(`alarm-${item.id}`);

    alarmMarkers.addFeature(alarmFeature);

}


export function triggerAlarmMarker(layer){
    var animationTween = new Tween(OpenLayers.Easing.Quad.easeInOut);
    var begin = {
      x: 5
    }
    var end = {
      x: 20
    }
    var callbacks = {
        eachStep: OpenLayers.Function.bind(animate, layer),
        done:function(){
          animationTween = new OpenLayers.Tween(OpenLayers.Easing.Quad.easeInOut);
          begin.x = (begin.x == 20 ? 5 : 20); 
          end.x = (end.x == 20 ? 5 : 20); 
          animationTween.start(begin, end, 80, {
            callbacks: callbacks
          });
        }
      }
    animationTween.start(begin, end, 80, {
      callbacks: callbacks
    });
    function animate(delta) {
        console.log(delta.x);
        for (var i = 0; i < this.features.length; i++) {
          this.features[i].style.strokeWidth = delta.x;
          this.features[i].style.pointRadius = delta.x;
        }
        this.redraw();
      }
  
}