// export async function getAllUsers() {

//     const response = await fetch('/api/users');
//     return await response.json();
// }

// export async function createUser(data) {
//     const response = await fetch(`/api/user`, {
//         method: 'POST',
//         headers: {'Content-Type': 'application/json'},
//         body: JSON.stringify({user: data})
//       })
//     return await response.json();
// }

// return the info true/false of the initial registration of the a
export async function getRegistrationInfo() {
    const response = await fetch('/api/registrationInfo');
    return await response.json();
}

export async function assetRegistration(data){
    const response = await fetch(`/api/registerAsset`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({assetId: data})
      })
    return await response.json();
}

export async function getAssetRegisteredInfo(){
    const response = await fetch('/api/assetRegisteredInfo');
    return await response.json();
}