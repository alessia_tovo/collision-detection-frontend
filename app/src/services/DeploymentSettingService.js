import { Asset, AssetType, EnvironmentPlan } from "../models/BaseModel";

export async function getAssetsList() {
    return await fetch('/registry/asset')
    .then(response => {
        if (response.status == 200) return response.json();
        else throw new Error(`API error in getAssetsList: statusCode = ${response.status}`);
    }
);
}

export async function getAssetsTypeList() {
    return await fetch('/registry/assetType')
    .then(response => {
        if (response.status == 200) return response.json();
        else throw new Error(`API error in getAssetsTypeList: statusCode = ${response.status}`);
    });
}



export async function getDeploymentSettings() {
    return await fetch('/registry/deployment')
    .then(response => {
        if (response.status == 200) return response.json();
        else throw new Error(`API error in getDeploymentSettings: statusCode = ${response.status}`);
    }
);
}

export async function getAllEnvironments(){
    return await fetch('/registry/environment')
        .then(response => {
            if (response.status == 200) return response.json();
            else throw new Error(`API error in getAllEnvironments: statusCode = ${response.status}`);
        }
    );
   
}


export async function getAllPlans(){
    return await fetch('/plan')
    .then(response => {
        if (response.status == 200) return response.json();
        else throw new Error(`API error in getAllPlans: statusCode = ${response.status}`);
    }
);

}


export async function getSinglePlanImage(id_env){
    return await fetch(`/plan/env/${id_env}/image`)
    .then(response => {
        if (response.status == 200) {
           return response.arrayBuffer();
        }
        else throw new Error(`API error in getSinglePlanImage with id_env=${id_env}: statusCode = ${response.status}`);
    })

}


export async function getEnvironmentPlanMap(){
    return getAllEnvironments().then(envJson => {
        return getAllPlans().then(planInfoJson => {

            let promises = Object.entries(envJson).flatMap(([env_id, env_info]) => {
                const plan_info = planInfoJson[env_id];

                if (plan_info != undefined && plan_info != null){
                   const promise =  getSinglePlanImage(env_id).then(bytesPlan => {
                        return new EnvironmentPlan(
                            env_id,
                            env_info.name,
                            env_info.measurements.xOriginMm,
                            env_info.measurements.yOriginMm,
                            env_info.measurements.widthMm,
                            env_info.measurements.heightMm,
                            0, // TODO: change field reading 
                            0, // same
                            plan_info.widthPx,
                            plan_info.heightPx,
                            bytesPlan
                        );
                    }).catch(error => {
                        console.log(`No plan image for env_id= ${env_id}: ${error}`);
                        return undefined;
                    })
                    return [promise];

                } else {
                    console.log(`Undefined or null plan_info for env_id:${env_id}`);
                    return [];
                }
            });

            return Promise.all(promises).then(envDescList => {
                return Object.fromEntries((envDescList.filter(x => x!= undefined)).map(x => [ x.id , x]));
            })

        }).catch(error => {
            console.log(`Error getting plans info: ${error}`);
            throw error;
        });
    }).catch(error => {
        console.log(`Error getting all environments: ${error}`);
        throw error;
    });
}


export async function getAssetsMap(){
    return getAssetsList().then(assetsJson => {
        let assetMap = {};
        for (const [key, asset] of Object.entries(assetsJson)) {
            assetMap[key] = new Asset(
                key, 
                asset.typeId,
                asset.name,
                asset.metadata
            ) 
        }
        return assetMap;
    
    }).catch(error => {
        console.log(`Not assets in getAssetsMap: ${error}`);
        return [];
    })
}

export async function getAssetTypeMap(){
    return getAssetsTypeList().then(assetTypesJson => {
        var assetTypeMap = {};
        for (const [key, assetType] of Object.entries(assetTypesJson)) {
            assetTypeMap[key] = new AssetType(
                key, 
                assetType.name,
                assetType.metadata
            )  
        }
        return assetTypeMap;
    }).catch(error => {
        console.log(`Not asset types in getAssetTypeMap: ${error}`);
        return [];
    })
}
