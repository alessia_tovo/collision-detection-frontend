import { Subject } from "rxjs";
import { Position, Motion, PredictedPositions } from "../models/BaseModel";


function computePredictedPositions(idEnv, asset_id, actualTimestamp, motionPredication){
    let predictions = new Map();
    for(const prediction of motionPredication){
        let futureTimestamp = actualTimestamp + (prediction.deltaSec * 1000 );
        let position = new Position(asset_id, idEnv, prediction.xMm, prediction.yMm, futureTimestamp);
        predictions.set(prediction.deltaSec, position);
        
    }

    return predictions;
}

export function setupDataSource(){

    let dataObserver = new Subject();

    var loc = window.location, new_uri;
    if (loc.protocol === "https:") {
        new_uri = "wss:";
    } else {
        new_uri = "ws:";
    }
    new_uri += "//" + loc.host;
    new_uri += loc.pathname + "collision/stream-enriched";
   
    console.log(new_uri)
    // const wsDataSource = new WebSocket('ws://192.168.10.160:8080/collision/stream-enriched');
    const wsDataSource = new WebSocket(new_uri);
    wsDataSource.onopen = function(data){

        console.log(`Opened websocket. Data received ${JSON.stringify(data)}`);
    }

    wsDataSource.onerror = function(event){
        console.log(`Error in websocket data source: ${JSON.stringify(event)}`);
    }

    wsDataSource.onclose = function(data){
        console.log(`Closing websocket data source: ${JSON.stringify(data)}`);

    }

    wsDataSource.onmessage = function(event){
    
        let data = JSON.parse(event.data);

        let position = new Position(data.assetId, data.position.env, data.position.xMm, data.position.yMm, data.timestamp);
        let motion = new Motion(data.assetId, data.motion.kind, data.motion.velocity, data.motion.heading);
        // let neighbors = new Set(Array.isArray(Object.keys(data.neighbors)) ? Object.keys(data.neighbors): [] );
        let neighbors = new Map(Array.isArray(Object.keys(data.neighbors))? Object.entries(data.neighbors) : []);
        let collisions = new Set(Array.isArray(data.collisions) ? data.collisions: []);
        let smoothed = new Position(data.assetId, data.smoothedPosition.env, data.smoothedPosition.xMm, data.smoothedPosition.yMm, data.timestamp);

        let predictedPositions = computePredictedPositions(data.position.env, data.assetId, data.timestamp, data.motionPredication);
        let prediction = new PredictedPositions(data.assetId, predictedPositions);


        
        // console.log(`Received position: ${position}, neighbors: ${Array.from(neighbors.values())}, collisions: ${Array.from(collisions.values())}`);
        
        dataObserver.next({
            'position': position,
            'neighbors': neighbors,
            'collisions': collisions,
            'motion': motion,
            'smoothed': smoothed,
            'prediction': prediction,
            'asset_id': data.assetId
        })

    }

    return dataObserver;
}

