var  { EnvironmentMap } = require('../models/EnvironmentMap')
import Feature from 'ol/Feature'
import Point from 'ol/geom/Point'
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';

var getRandomColorByString = function (str) {
    var hash = 0;
    for (var j = 0; j < str.length; j++) {
        hash = str.charCodeAt(j) + ((hash << 5) - hash);
    }

    var colour = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
};

export function createEnvSettingClassFromResponse(environmentSettings){

    var idMap = `env-${environmentSettings.id}`;

    var envMap = new EnvironmentMap(
        idMap,
        environmentSettings.name,
        environmentSettings.measurements.xOriginMm,
        environmentSettings.measurements.yOriginMm,
        environmentSettings.measurements.widthMm,
        environmentSettings.measurements.heightMm,
        )

    return envMap;

}

export function adjustMapSize(){
   
    var navbarElement = document.getElementById('navbar-id-app')
    var navBarHeight = navbarElement.offsetHeight;
    if (!navBarHeight) navBarHeight = 0;

    var footerElement = document.getElementById('footer-id-app');
    var footerHeight =  footerElement.offsetWidth;
    if (!footerHeight) footerHeight = 0;

    var widthHeight =screen.height;
    var height = widthHeight - navBarHeight - footerHeight-100;
       
    return height;
}

export function showAssetOnMap(item, map, environmentSettings, sourceMarkers, assetsMap){
    var marker;    

    var markerId = item.id;
    // var id_env = item.position.coordinates.id_environment;
    // console.log(Object.keys(sourceMarkers));
    if(sourceMarkers.getFeatureById(markerId)){
        console.log('OLD marker');
        // if marker for this asset is already shown in map, change its information
        marker = sourceMarkers.getFeatureById(markerId);


        marker.getGeometry().setCoordinates([item.position.coordinates.x, item.position.coordinates.y]);

        var newCoordinates = item.position.coordinates;
        marker.set('coordinates', newCoordinates);





    } else {
        // if marker for this asset has not been created, create a new one and save it
        var coordinates = item.position.coordinates;

        var asset = assetsMap[item.id]!=null ? assetsMap[item.id] : null;
        
        marker = new Feature({
            id: item.id,
            asset: asset,
            coordinates: coordinates,
            trace: [[coordinates.x, coordinates.y]],
            geometry: new Point([coordinates.x, coordinates.y])
        });

        if (marker.get('asset') && marker.get('asset').typeId) {
            var markerColor = '#8959A8';

            if (marker.get('asset') && marker.get('asset').name) {
                markerColor = getRandomColorByString(marker.get('asset').name);
            }
            marker.set('color', markerColor);

            marker.setId(markerId);
            marker.setStyle(new Style({
                image: new Icon({
                    opacity: 1,
                    anchor: [0.5, 0.5],
                    scale: 1,
                    anchorXUnits: "fraction",
                    anchorYUnits: "fraction",
                    color: markerColor,
                    crossOrigin: 'anonymous',
                    src: 'https://openlayers.org/en/v3.20.1/examples/data/dot.png'

                })
            }))
            sourceMarkers.addFeature(marker);
            console.log('NEW marker!');
        }
    }
  
    
}
