import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      component: () => import("@/views/Dashboard")
     },
    {
      name: "register",
      path: "/register",
      component: () => import("@/views/Register")
    },
    {
      name: 'removeAssociation',
      path: "/removeAssociation",
      component: () => import("@/views/RemoveAssociation")
    }
    
  ]
});
