/** asset class and assets Map */

export class Asset {
    id;
    typeId;
    name;
    metadata;

    constructor(id, typeId, name, metadata){
        this.id = id;
        this.typeId = typeId;
        this.name = name;
        this.metadata = metadata;
    }

    toString(){
        return `Asset(${this.id}, ${this.name}, ... )`;
    }
}



/** position class and asset position map */

export class Position {
    asset_id
    env_id;
    x;
    y;
    timestamp;

    constructor(asset_id, env_id, x, y, timestamp){
        this.asset_id = asset_id;
        this.env_id = env_id;
        this.x = x;
        this.y = y;
        this.timestamp = timestamp;
    }

    toString(){
        return `Position(${this.asset_id}, ${this.env_id}, ${this.x}, ${this.y}, ${this.timestamp})`;
    }
}


export class PredictedPositions {
    asset_id;
    positions;

    constructor(asset_id, positions){
        this.asset_id =  asset_id;
        this.positions = positions;
    }

    toString(){
        return `${this.asset_id} predicted positions ${this.positions}`
    }
}





export class Motion {
    asset_id;
    kind;
    velocity;
    heading;

    constructor(asset_id, kind, velocity, heading){
        this.asset_id = asset_id;
        this.kind = kind,
        this.velocity = velocity;
        this.heading = heading
    }

    toString(){
        return `Motion of asset_id=${this.asset_id}, kind=${this.kind}, velocity=${this.velocity}, heading=${this.heading}`;
    }
}

export class AssetType {
    id;
    name;
    metadata;

    constructor(id, name, metadata){
        this.id = id;
        this.name = name;
        this.metadata = metadata;
    }

}


export class EnvironmentPlan {
    id;
    name;
    x_origin;
    y_origin;
    width;
    height;
    bg_image_extent;
    plan_x_offset;
    plan_y_offset;
    plan_width;
    plan_height;
    plan_bytes;


    constructor(id, name, x_origin, y_origin, width, height, plan_x_offset, plan_y_offset, plan_width, plan_height, plan_bytes) {
        
        this.id = id;
        this.name = name;
        this.x_origin = x_origin;
        this.y_origin = y_origin;
        this.width = width;
        this.height = height;

        this.plan_x_offset = plan_x_offset;
        this.plan_y_offset = plan_y_offset;
        this.plan_width = plan_width;
        this.plan_height = plan_height;
        this.plan_bytes = plan_bytes;
    }


    getImageDataBase64() {
        var base64 = btoa(
            new Uint8Array(this.plan_bytes).reduce((data, byte) => data + String.fromCharCode(byte), '')
          );
          var data = 'data:image/png;base64,' + base64;
          return data;
    }

    getBgImageExtent(){
        return  [-1 * this.plan_x_offset , this.plan_y_offset -this.height, -1 * this.plan_x_offset + this.width, this.plan_y_offset]
    }


}



export class Registry {
    #itemsByKey;
    #keyF;
    /** kf: item => key */ 
    constructor(keyF, items){
        this.#itemsByKey = new Map();
        this.#keyF=keyF;
        this.updateAll(items);
    }

    update(item){
        let key = this.#keyF(item);
        this.#itemsByKey.set(key, item);
    }

    updateAll(items){
        for(const item of items){
            this.update(item);
        }
    }

    values(){
        return this.#itemsByKey.values();
    }

    get(key){
        return this.#itemsByKey.get(key);
    }

    size(){
        return this.#itemsByKey.size;
    }
}


