module.exports = {
    devServer: {
      proxy: {
        '^/api': {
          target: 'http://localhost:3080',
          changeOrigin: true
        },
        '^/registry': {
          target: 'http://localhost:8123',
          changeOrigin: true
        },
        '^/collision': {
          target: 'http://localhost:8123',
          changeOrigin: true,
          ws: true
        },
        '^/plan': {
          target: 'http://localhost:8123',
          changeOrigin: true
        },

      }
    }
  }